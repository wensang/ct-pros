# Contributing Doc <!-- omit in toc -->
Guide for contributors. See references for more details on the different sections.

## Index <!-- omit in toc -->
- [Rules](#rules)
- [Pipeline (TDD)](#pipeline-tdd)
- [Structure](#structure)
- [Guides](#guides)
  - [Git](#git)
  - [Python Virtual Environments](#python-virtual-environments)
- [References](#references)
- [Return to README](#return-to-readme)


## Rules
- Format Style: `black`
- Tests: `passing`
- Coverage: `>=previous`

## Pipeline (TDD)
1. Submit bug or feature.
2. Encompass expected behavior of bug or feature with tests in corresponding part of code structures.
3. Add source code to generate expected behavior and mirrored positions from the test codebase.

## Structure
The structure followed by this project prioritizes compatibility with `pip` editable installs, test-driven development, and similarity to suggested project structures.

```
{ctpros} (project root folder)
└───data/ (data storage for tests)
│   └───input/ (raw unprocessed data)
│   └───interim/ (temp intermediate files)
│   └───output/ (resulting original-space and quantified data tables)
│   └───supplement/ (supplementary data for input data)
│
└───docs/ (workflow documents, standard operating procedure guidelines)
│
└───notebooks/ (Jupyter Notebooks working exclusively from data/*/ to generate visualizations and summaries)
│
└───refs/ (referenced files/resources in docs)
│
└───src/ (all source code)
|   │
|   └───ctpros/ (main directory of module code)
|       └───__main__.py (Defines module call behavior)
|       └───__init__.py (Defines top-level API)
|       |
|       └───graphics/ (GUI-based functions)
|       |   └───components/ (GUI components defined and placed through `__main__.py`)
|       |   |   └───backend.py (application level API)
|       |   |   └───imgframe.py (image slice panels)
|       |   |   └───infoframe.py (information panel)
|       |   |   └───menu.py (application menu)
|       |   |   └───progressbar.py (progressbar implementation)
|       |   |   └───tools.py (Tk components)
|       |   |
|       |   └───__init__.py (empty init to recognize graphics as submodule)
|       |   └───__main__.py (contains front-end outline of main application and other top-level windows)
|       |
|       └───img/ (Image classes and functions, such as IO and processing)
|       |   └───utils/ (Image helper functions)
|       |   └───__init__.py (Defines img API)
|       |   └───classes.py (contains the image template and image IO specific classes)
|       |   └───methods.py (contains all image processing components)
|       |
|       └───protocols/ (scripts for common imaging pipelines)
|           └───stitch.py (resampling and averaging of pre-registered images)
│
└───tests/ (unit tests for source code)
│   └───test_ctpros/ (tests encompassing src/ctpros)
│   └───* (further test files mirroring structure of ./src/)
│
└───.coveragerc (Python coverage tool instructions)
└───.gitlab-ci.yml (CI/CD pipeline instructions for GitLab)
└───LICENSE.txt (license information)
└───requirements.txt (Python-specific dev and CI/CD pipeline dependencies)
└───setup.py (Manages compilation of Python package via *pip*)
└───**.MD (markdown documentation)

Verbosity Levels:
0: Exceptions
1: Warnings
2: Friendly API responses
3: GUI-level responses
```
## Guides
### Git
Step-by-step to be done. You will likely want to fork your own version.
Where ${branch-name} is ${issue-number}-issue-description (associating branch to the issue):

Setting up your local repository:
```bash
git init .
git remote add upstream git@gitlab.com:caosuna/ctpros
git remote add origin git@gitlab.com:${username}/ctpros
git pull  origin master
git checkout -b dev ${branch-name} # work on a branch associated to the issue of interest
```

Updating your fork:
```bash
git fetch upstream
git rebase upstream/master
# git push --set-upstream origin ${branch-name} # if first time pushing updates to this branch
git push # for all following pushes of that branch
```
Finally, follow-up with a pull-request to the parent remote repository.


### Python Virtual Environments
Virtual environments help isolate and organize dependencies between projects.
Here are examples of what to write in your console to set this up:

#### Windows <!-- omit in toc -->
In your `ctpros` project folder:
```bash
setx PIP_REQUIRE_VIRTUALENV=true # prevent pip from installing on global installs
venv venv # generate virtual environment
.\venv\Scripts\activate # activate virtual environment
pip install -e . # editable installation of ctpros
pip install -r requirements.txt # install dev-specific dependencies
```
You are now ready to get testing! Verify your installation worked by running: `coverage run` then `coverage report`.
You should see the results of all of the tests run as well as the code coverage of the tests on the code base. Find a readable HTML report at `.artifacts\coverage\coverage_html\index.html`.

### Auto-Documentation w/ Sphinx
Sphinx is used to build styled HTML webpages of documentation from source code documentation to improve sustainability of updated documentation.

#### Setup
Following installation of dev dependencies and virtual environment:
```bash
#./
call venv/Scripts/activate
sphinx-quickstart .artifacts/sphinx -q -p ctpros -a "Carlos Osuna" --ext-autodoc --ext-todo --ext-viewcode
sphinx-apidoc -o .artifacts/sphinx src/ctpros
call .artifacts/sphinx/make html
```
## References
- [Getting started with Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
- [Working with Forks](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
- [Python Virtual Environments](https://docs.python-guide.org/dev/pip-virtualenv/)

## Sphinx
```bash
sphinx-quickstart docs/sphinx -q -p ctpros -a "Carlos Osuna" --ext-autodoc --ext-todo --ext-viewcode
# modify .artifacts/sphinx/conf.py:50 --> `html_theme = "pydata_sphinx_theme"`
# modify .artifacts/sphinx/index.rst:13 --> `   modules`
```
## Return to [README](../README.md)