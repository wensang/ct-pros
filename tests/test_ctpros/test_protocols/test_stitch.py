import unittest, os
import numpy as np

import ctpros as ct
from ctpros.protocols.stitch import *


class TestSet(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing

    def setUp(self):
        array1 = ct.NDArray((3, 3, 3), verbosity=False)
        array1.affine.scale(2)
        array2 = ct.NDArray((3, 3, 3), verbosity=False)
        array2.affine.scale(2)
        array1[:] = 0
        array2[:] = 0
        for i in range(3):
            array1[i, i] = 1
            array2[i, i] = 2
        array2.affine.translate(-2, -2, -2)
        self.array1 = array1
        self.array2 = array2

    def tearDown(self):
        pass

    def test_stitch(self):
        actual = stitcher(self.array1, self.array2)
        expected = [
            [
                [2.0, 2.0, 2.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.0, 0.0, 0.0],
                [2.0, 1.5, 1.5, 1.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [2.0, 1.5, 1.5, 1.0],
                [0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0],
                [0.0, 1.0, 1.0, 1.0],
            ],
        ]
        self.np.assert_equal(actual, expected)

    def test_reorient_to_average(self):
        tfm = reorient(self.array1, self.array2)
        self.np.assert_equal(ct.AffineTensor(3).translate(1, 1, 1), tfm)

    def test_voi_calculates_proper_sampleinfo(self):
        tfm = ct.AffineTensor(3).translate(1, 1, 1)
        voi = define_voi([self.array1, self.array2], tfm)
        self.np.assert_equal(voi.shape, [4, 4, 4])
        self.np.assert_equal(voi.elsize, [2, 2, 2])
        self.np.assert_equal(voi.pos, [[-1], [-1], [-1]])

    def test_reorient_to_None(self):
        tfm = reorient(self.array1, self.array2, tfm=None)
        self.np.assert_equal(ct.AffineTensor(3), tfm)

    def test_align(self):
        img1 = ct.ImgTemplate("data/input/test_v2.aim;1")
        img2 = ct.ImgTemplate("data/input/test_v3.aim;1")
        align(img1, img2, reference=img1)

        expected1 = ct.AffineTensor(3).scale(6)
        expected_inv = (
            ct.AffineTensor(3).rotate(0.01, 0.02, 0.03).translate(1, 2, 3)
        ).inv()
        self.np.assert_almost_equal(img1.affine, expected1)
        self.np.assert_almost_equal(expected_inv.rotate(), img2.affine.rotate())
        self.np.assert_almost_equal(expected_inv.translate(), img2.affine.translate())

    def test_reorient_to_tfm(self):
        tfm = ct.AffineTensor(3).translate(2, 2, 2)
        reorienter = tfms = reorient(self.array1, self.array2, tfm=tfm)
        self.np.assert_equal(tfm.copy().inv(), reorienter)
        self.np.assert_equal(tfm.copy().inv(), reorienter)
