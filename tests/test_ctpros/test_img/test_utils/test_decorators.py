import unittest

from ctpros.img.utils.decorators import *


class PlaceHolder(np.ndarray):
    @inplace
    def returns_array_or_values(self, type):
        if type == "array":
            return np.ones_like(self, dtype=np.uint8)
        else:
            return 3


class TestSet(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing

    def setUp(self):
        pass

    def test_inplace_array(self):
        array = PlaceHolder(1)
        array[0] = 0
        returns = array.returns_array_or_values("array")
        self.np.assert_equal(returns, array)
        self.assertEqual(array[0], 1)
        self.assertEqual(array.dtype, np.uint8)

    def test_inplace_value(self):
        array = PlaceHolder(1)
        returns = array.returns_array_or_values("values")
        self.assertNotEqual(array[0], 3)
        self.assertEqual(returns, 3)

    def test_inplace_ofview(self):
        array = PlaceHolder(1)
        array[:] = np.pi / 3
        newarray = np.sin(array)
        with self.assertRaises(Exception):
            newarray.returns_array_or_values("array")

    def tearDown(self):
        pass
