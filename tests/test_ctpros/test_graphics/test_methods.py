import unittest, os
import numpy as np

from ctpros import img
from ctpros.graphics.main import *


class TestSet_File(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.GUI = lambda *imgs: GUI(*imgs, withdraw=True, verbosity=False)
        self.npy2name = os.path.sep.join(["data", "input", "test_v2.npy"])
        self.npy3name = os.path.sep.join(["data", "input", "test_v3.npy"])
        self.aim3name = os.path.sep.join(["data", "input", "test_v3.aim;1"])
        self.aim2name = os.path.sep.join(["data", "input", "test_v2.aim;1"])
        self.aim2gobj = os.path.sep.join(["data", "input", "test_v2_gobj.aim;1"])
        self.affine = os.path.sep.join(["data", "input", "test_affine.tfm"])
        self.voi = os.path.sep.join(["data", "input", "test_voi.voi"])
        self.savedir = os.path.sep.join(["data", "output"]) + os.path.sep
        infolders = ["data", "input"]
        interimfolders = ["data", "interim"]
        suppfolders = ["data", "supplement"]
        outfolders = ["data", "output"]
        self.dirin = os.path.abspath(os.path.join(*infolders)) + os.path.sep
        self.dirinterim = os.path.abspath(os.path.join(*interimfolders)) + os.path.sep
        self.dirout = os.path.abspath(os.path.join(*outfolders)) + os.path.sep
        self.dirsupp = os.path.abspath(os.path.join(*suppfolders)) + os.path.sep

    def setUp(self):
        pass

    def tearDown(self):
        """
        Empty interim and output test datafolders

        """
        for folder in [self.dirinterim, self.dirout]:
            for root, _, files in os.walk(folder):
                for f in files:
                    if f.lower() != "readme.md":
                        os.unlink(os.path.join(root, f))

    def test_open_npy(self):
        gui = self.GUI()
        gui.menu.file_open_image(self.npy2name)
        self.assertEqual(gui.get_selected_imgnames()[0], gui.imgs[0].filename)
        self.np.assert_equal(gui.get_pos(), gui.imgs[0].position)
        self.np.assert_equal(gui.get_voi().pos, gui.imgs[0].voi.pos)
        self.np.assert_equal(gui.get_voi().shape, gui.imgs[0].voi.shape)
        self.np.assert_equal(gui.get_voi().elsize, gui.imgs[0].voi.elsize)
        gui.flag_render.set(True)
        self.assertTrue(gui.traframe.arrays)
        self.assertTrue(gui.corframe.arrays)
        self.assertTrue(gui.sagframe.arrays)

    def test_open_img2(self):
        gui = self.GUI()
        gui.menu.file_open_image(self.aim3name, self.aim2name)
        self.assertEqual(
            gui.get_selected_imgs()[0].filename, os.path.basename(self.aim3name)
        )
        self.assertEqual(
            gui.get_selected_imgs()[1].filename, os.path.basename(self.aim2name)
        )
        self.assertNotEqual(gui.imgs[0].nbytes, 0)
        self.assertNotEqual(gui.imgs[1].nbytes, 0)

    def test_open_img3(self):
        gui = self.GUI()
        filenames = [self.aim3name, self.aim2name, self.aim2gobj]
        gui.menu.file_open_image(*filenames)
        self.assertNotEqual(gui.imgs[0].nbytes, 0)
        self.assertNotEqual(gui.imgs[1].nbytes, 0)
        self.assertEqual(gui.imgs[2].nbytes, 0)

    def test_close_img(self):
        gui = self.GUI()
        gui.menu.file_open_image(self.aim3name)
        gui.menu.file_close_image(0)
        self.assertEqual(gui.get_selected_imgs(), [])

    def test_close_img2(self):
        gui = self.GUI()
        filenames = [self.aim3name, self.aim2name]
        gui.menu.file_open_image(*filenames)
        gui.menu.file_close_image(0)
        gui.menu.file_close_image(0)
        self.assertEqual(gui.get_selected_imgs(), [])

    def test_close_img2to1(self):
        gui = self.GUI()
        filenames = [self.aim3name, self.aim2name]
        gui.menu.file_open_image(*filenames)
        gui.menu.file_close_image(0)
        self.assertIsNotNone(gui.get_selected_imgs()[0])

    def test_close_all(self):
        gui = self.GUI()
        filenames = [self.aim3name, self.aim2name]
        gui.menu.file_open_image(*filenames)
        gui.menu.file_close_all_image()
        self.assertFalse(gui.imgs)
        self.assertFalse(gui.get_selected_imgs())

    def test_close_extra(self):
        gui = self.GUI()
        filenames = [self.aim3name, self.aim2name, self.aim2gobj]
        gui.menu.file_open_image(*filenames)
        gui.menu.file_close_image(2)
        self.assertEqual(len(gui.imgs), 2)
        self.np.assert_equal(
            gui.get_selected_imgnames(), [gui.imgs[0].filename, gui.imgs[1].filename]
        )

    def test_nonuniquenames(self):
        gui = self.GUI()
        with self.assertRaises(Exception):
            gui.menu.file_open_image(self.aim3name, self.aim3name)

    def test_affine_apply(self):
        aim = img.AIM(self.aim3name, verbosity=1)
        affine = img.AffineTensor(self.affine)
        expected = np.dot(affine, aim.affine)
        gui = self.GUI(aim)
        gui.menu.file_apply_affine(0, affinename=self.affine)
        self.np.assert_equal(expected, aim.affine)

    def test_affine_overwrite(self):
        primary = img.AIM(self.aim3name, verbosity=1)
        secondary = img.AIM(self.aim2name, verbosity=1)
        affine = img.AffineTensor(self.affine)
        primary.affine.rotate(0.1, 0.2, 0.3)
        secondary.affine.scale(0.5)
        secondary.affine.translate(-0.1, -0.2, -0.3)
        p, s = [
            img.AffineTensor(3).align(affine)
            for affine in [primary.affine, secondary.affine]
        ]
        expected_diff = np.dot(s, affine)
        expected = np.dot(expected_diff, primary.affine)
        gui = self.GUI(primary, secondary)
        gui.menu.file_apply_affine_relative(0, affinename=self.affine)
        self.np.assert_equal(expected, primary.affine)

    def test_affine_save(self):
        aim = img.AIM(self.aim3name, verbosity=1)
        expected = img.AffineTensor(3).rotate(
            *(np.pi * np.array([1 / 4, 1 / 3, 1 / 5]))
        )
        aim.affine.affine(expected)
        savename = self.savedir + "affine.tfm"

        gui = self.GUI(aim)
        gui.menu.file_save_affine(0, affinename=savename)
        actual = img.AffineTensor(savename)
        self.np.assert_almost_equal(expected, actual)

    def test_voi_open(self):
        aim = img.AIM(self.aim3name, verbosity=1)
        gui = self.GUI(aim)
        gui.menu.file_open_voi(self.voi)
        self.assertEqual(gui.voi["elsize"][0].get(), 10)

    def test_voi_save(self):
        aim = img.AIM(self.aim3name, verbosity=1)
        gui = self.GUI(aim)
        savename = self.savedir + "voi.voi"
        gui.menu.file_save_voi(savename)
        actual = img.VOI(savename)
        expected = aim.voi
        self.np.assert_equal(actual.pos, expected.pos)
        self.np.assert_equal(actual.shape, expected.shape)
        self.np.assert_equal(actual.elsize, expected.elsize)

    def test_crop_save(self):
        aim = img.AIM(self.aim3name, verbosity=1)
        savename = self.savedir + "test_v3_crop.aim"

        gui = self.GUI(aim)
        aim.voi.shape = aim.voi.shape // 3
        view = gui.menu.file_save_crop(0, savename)
        self.np.assert_equal(view.affine, img.AffineTensor(3).scale(6))
        self.np.assert_equal(view.shape, (15, 9, 10))


class TestSet_Register(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.GUI = lambda *imgs: GUI(*imgs, withdraw=True, verbosity=False)
        self.npy2name = os.path.sep.join(["data", "input", "test_v2.npy"])
        self.npy3name = os.path.sep.join(["data", "input", "test_v3.npy"])
        self.aim3name = os.path.sep.join(["data", "input", "test_v3.aim;1"])
        self.aim2name = os.path.sep.join(["data", "input", "test_v2.aim;1"])
        self.aim3twist = os.path.sep.join(["data", "input", "test_v3_twistedcrop.aim"])
        self.aim2gobj = os.path.sep.join(["data", "input", "test_v2_gobj.aim;1"])
        self.affine = os.path.sep.join(["data", "input", "test_affine.tfm"])
        self.voi = os.path.sep.join(["data", "input", "test_voi.voi"])
        self.savedir = os.path.sep.join(["data", "output"]) + os.path.sep
        infolders = ["data", "input"]
        interimfolders = ["data", "interim"]
        suppfolders = ["data", "supplement"]
        outfolders = ["data", "output"]
        self.dirin = os.path.abspath(os.path.join(*infolders)) + os.path.sep
        self.dirinterim = os.path.abspath(os.path.join(*interimfolders)) + os.path.sep
        self.dirout = os.path.abspath(os.path.join(*outfolders)) + os.path.sep
        self.dirsupp = os.path.abspath(os.path.join(*suppfolders)) + os.path.sep

    def test_pass(self):
        pass


class TestSet_Transform(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing
        self.GUI = lambda *imgs: GUI(*imgs, withdraw=True, verbosity=False)
        self.npy2name = os.path.sep.join(["data", "input", "test_v2.npy"])
        self.npy3name = os.path.sep.join(["data", "input", "test_v3.npy"])
        self.aim3name = os.path.sep.join(["data", "input", "test_v3.aim;1"])
        self.aim2name = os.path.sep.join(["data", "input", "test_v2.aim;1"])
        self.aim2gobj = os.path.sep.join(["data", "input", "test_v2_gobj.aim;1"])
        self.affine = os.path.sep.join(["data", "input", "test_affine.tfm"])
        self.voi = os.path.sep.join(["data", "input", "test_voi.voi"])
        self.savedir = os.path.sep.join(["data", "output"]) + os.path.sep
        infolders = ["data", "input"]
        interimfolders = ["data", "interim"]
        suppfolders = ["data", "supplement"]
        outfolders = ["data", "output"]
        self.dirin = os.path.abspath(os.path.join(*infolders)) + os.path.sep
        self.dirinterim = os.path.abspath(os.path.join(*interimfolders)) + os.path.sep
        self.dirout = os.path.abspath(os.path.join(*outfolders)) + os.path.sep
        self.dirsupp = os.path.abspath(os.path.join(*suppfolders)) + os.path.sep

    def setUp(self):
        pass

    def tearDown(self):
        pass
